﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
        public class Calculator
        {
            public double Calculate(string s)
            {
                for(int i = 0; i < s.Length; i++)
            {
                if (s[i] == '+' && s[i + 1] == '-' || s[i] == '-' && s[i + 1] == '+')
                    if (s[i] == '+')
                       s = s.Remove(i, 1);
                    else
                       s = s.Remove(i + 1, 1);

                  
            }
                s = '(' + s + ')'; 
                Stack<double> operands = new Stack<double>();
                Stack<char> functions = new Stack<char>();
                int pos = 0;
                object token;
                object prevToken = 'Ы';
               
                do
                {
                    token = GetToken(s, ref pos);
               
                if (token is char && prevToken is char &&
                        (char) prevToken == '(' &&
                        ((char) token == '+' || (char) token == '-'))
                        operands.Push(0); // Добавляем нулевой элемент

                        if (token is double ) // Если операнд
                        {
                            operands.Push((double) token); // засовывам в стек
                        }
                        
                else if (token is char) // Если операция
                        {
                            if ((char) token == ')') 
                            {                              
                                while (functions.Count > 0 && functions.Peek() != '(')
                                    PopFunction(operands, functions);
                                    functions.Pop(); 
                            }
                            else
                            {
                                while (CanPop((char) token, functions)) 
                                    PopFunction(operands, functions); 

                                    functions.Push((char) token); 
                            }
                        }
                        prevToken = token;
                }
                while (token != null);

                if (operands.Count > 1 || functions.Count > 0)
                    throw new InvalidOperationException("Ошибка в разборе выражения");

                return operands.Pop();
            }

            private void PopFunction(Stack<double> operands, Stack<char> functions)
            {
                double b = operands.Pop();
                double a = operands.Pop();
                switch (functions.Pop())
                {
                    case '+':
                        operands.Push(a + b);
                        break;
                    case '-':
                        operands.Push(a - b);
                        break;
                    case '*':
                        operands.Push(a * b);
                        break;
                    case '/':
                        operands.Push(a / b);
                        break;
                }
            }

            private bool CanPop(char op1, Stack<char> functions)
            {
                if (functions.Count == 0)
                    return false;
                int p1 = GetPriority(op1);
                int p2 = GetPriority(functions.Peek());

                return p1 >= 0 && p2 >= 0 && p1 >= p2;
            }

            private int GetPriority(char op)
            {
                switch (op)
                {
                    case '(':
                        return -1; 
                    case '*':
                    case '/':
                        return 1;
                    case '+':
                    case '-':
                        return 2;
                    default:
                        throw new Exception("недопустимая операция");
                }
            }

            private object GetToken(string s, ref int pos)
            {
                ReadWhiteSpace(s, ref pos);

                if (pos == s.Length) 
                    return null;
                if (char.IsDigit(s[pos]))
                    return Convert.ToDouble(ReadDouble(s, ref pos));
                else
                    return ReadFunction(s, ref pos);
            }

            private char ReadFunction(string s, ref int pos)
            {             
                return s[pos++];
            }

            private string ReadDouble(string s, ref int pos)
            {
                string res = "";
                while (pos < s.Length && (char.IsDigit(s[pos]) || s[pos] == '.'))
                    res += s[pos++];

                return res;
            }

            // Считываем все проблемы и прочие символы.
            private void ReadWhiteSpace(string s, ref int pos)
            {
                while (pos < s.Length && char.IsWhiteSpace(s[pos]))
                    pos++;
            }
        }
}
