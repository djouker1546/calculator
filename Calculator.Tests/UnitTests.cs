﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Calculator.Tests
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var text = "2 + 3";
           var result = new Calculator().Calculate(text);
            Assert.AreEqual(result, 5);
        }

        [TestMethod]
        public void TestMethod2()
        {
            var text = "(2 + 3) * 6";
            var result = new Calculator().Calculate(text);
            Assert.AreEqual(result, 30);
        }

        [TestMethod]
        public void TestMethod3()
        {
            var text = "()2+3)*6";
            Assert.ThrowsException<InvalidOperationException>(() =>
            {
                new Calculator().Calculate(text);
            });
        }

        [TestMethod]
        public void TestMethod4()
        {
            var text = "2+-6";
            var result = new Calculator().Calculate(text);
            Assert.AreEqual(result, -4);
        }

        [TestMethod]
        public void TestMethod41()
        {
            var text = "2-+6";
            var result = new Calculator().Calculate(text);
            Assert.AreEqual(result, -4);
        }

        [TestMethod]
        public void TestMethod5()
        {
            var text = "-(33*11)/-11";
           
            Assert.ThrowsException<InvalidOperationException>(() =>
            {
                new Calculator().Calculate(text);
            });
        }

        [TestMethod]
        public void TestMethod6()
        {
            var text = "-(33*11)/(-11)";
            var result = new Calculator().Calculate(text);
            Assert.AreEqual(result, 33);
        }

        [TestMethod]
        public void TestMethod7()
        {
            var text = "-2";
            var result = new Calculator().Calculate(text);
            Assert.AreEqual(result,-2);
        }

        [TestMethod]
        public void TestMethod8()
        {
            var text = "2//2";
            Assert.ThrowsException<InvalidOperationException>(() =>
            {
                new Calculator().Calculate(text);
            });
        }

        [TestMethod]
        public void TestMethod9()
        {
            var text = "2+3*5";
            var result = new Calculator().Calculate(text);
            Assert.AreEqual(result, 17);
        }

        [TestMethod]
        public void TestMethod10()
        {
            var text = "(-(+2-1))";
            var result = new Calculator().Calculate(text);
            Assert.AreEqual(result, -1);
        }

        
    }
}
